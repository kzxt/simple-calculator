package main;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class CalculatorTest {
    private Calculator testSubject;

    @Before
    public void setUp() throws Exception {
        this.testSubject = new Calculator();
    }

    @After
    public void tearDown() throws Exception {
        testSubject = null;
    }

    @Test
    public void add() {
        Assert.assertEquals("Adding", 32, testSubject.add(12, 20));
    }

    @Test
    public void testAddNegativeNumbers () {
        Assert.assertEquals("Adding negative numbers", -5, testSubject.add(-2, -3));
    }

    @Test
    public void testAddNegativePositiveNumbers () {
        Assert.assertEquals("Adding negative and positive numbers", 1, testSubject.add(-2,  3));
    }

    @Test
    public void testMultiplyNegativeNumbers () {
        Assert.assertEquals("Multiplying negative numbers", 4, testSubject.multiply(-2, -2));
    }

    @Test
    public void testMultiplyPositiveNumbers () {
        Assert.assertEquals("Multiplying positive numbers", 4, testSubject.multiply(2, 2));
    }
}